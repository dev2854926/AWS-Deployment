# React App Deployment Pipeline to AWS S3

This is a GitLab CI/CD pipeline for deploying a React app into an AWS S3 bucket. The pipeline consists of several stages, including building, testing, and deploying to both staging and production environments. This README provides an overview of the pipeline configuration and steps to set up and use it.

## Pipeline Configuration

### Stages

The pipeline is divided into the following stages:

- **Build:** Build the React app and create a version file.
- **Test:** Test the React app by running it locally with `serve` and performing checks.
- **Deploy Staging:** Deploy the app to the staging environment.
- **Deploy Production:** Deploy the app to the production environment (manual approval required).

### Variables

- `APP_VERSION`: The version of the app, which is set to the CI/CD pipeline's ID (`$CI_PIPELINE_IID`).

### Jobs

#### Build Website

This job builds the React app, runs linting, tests, and generates a version file.

#### Test Website

This job tests the React app by deploying it locally with `serve`, waiting for it to start, and then checking if it's working correctly.

#### Deploy to Staging

This job deploys the built app to the staging environment in an AWS S3 bucket. It uses the AWS CLI to synchronize the local build with the S3 bucket.

#### Deploy to Production

This job deploys the app to the production environment, but it's a manual action that requires approval. This can help prevent accidental production deployments.

## Getting Started

To set up and use this pipeline for deploying your React app to AWS S3, follow these steps:

1. **Configure Environment Variables:**
   - Make sure you have the necessary environment variables set in your GitLab project:
     - `AWS_ACCESS_KEY_ID`: AWS access key ID with permissions to upload to the S3 bucket.
     - `AWS_SECRET_ACCESS_KEY`: AWS secret access key associated with the access key ID.
     - `AWS_DEFAULT_REGION`: AWS region where your S3 bucket is located.
     - `AWS_S3_BUCKET`: The name of your AWS S3 bucket.

2. **Update React App Configuration:**
   - Make sure your React app's configuration (e.g., API endpoints) is correctly set up for the staging and production environments.

3. **GitLab Repository Configuration:**
   - Ensure your GitLab repository is configured with appropriate CI/CD settings and variables.

4. **Commit Your Code:**
   - Push your React app code to your GitLab repository.

5. **Create Merge Requests:**
   - Create merge requests to trigger the pipeline for your branches. The pipeline will automatically deploy to staging when changes are pushed to branches other than the default branch. To deploy to production, you'll need to manually approve the deployment job.

6. **Monitor and Verify:**
   - Monitor the pipeline's progress in your GitLab project's CI/CD section.
   - Verify the deployed app in the staging and production environments.

With this pipeline, you can automate the deployment of your React app to both staging and production environments on AWS S3, helping you streamline your development and deployment process.

Remember to handle sensitive information like AWS access keys securely and consider using GitLab CI/CD environment variables for added security.
